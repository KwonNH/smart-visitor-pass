import os
import cv2
import dlib
import numpy as np
import argparse
from contextlib import contextmanager
from wide_resnet import WideResNet
from keras.utils.data_utils import get_file
import threading
import time

import pymysql
import pandas as pd

import pandas as pd
from time import localtime, strftime

pretrained_model = 'C:/Users/KWON/PycharmProjects/Smart-Visitor-Pass/deep_learning/age-gender-estimation/fine_tuned_model/WRN_16_8_2.h5'
modhash = '89f56a39a78454e96379348bddd78c0d'

faceCascade = cv2.CascadeClassifier('C:/Users/KWON/venv/tensorflow2/Lib/site-packages/cv2/data/haarcascade_frontalface_default.xml')

conn = pymysql.connect(host='smartvisitorpass.cwbrhvpulmkd.ap-northeast-2.rds.amazonaws.com',
                       port=3306, user='root', passwd='team21sangha', db='smart_visitor_pass', charset='utf8')

# The desired output width and height
OUTPUT_SIZE_WIDTH = 775
OUTPUT_SIZE_HEIGHT = 600

columns = ['id', 'face_id', 'age', 'gender', 'timestamp']
result_df = pd.DataFrame(columns=columns)


def get_args():
    parser = argparse.ArgumentParser(description="This script detects faces from web cam input, "
                                                 "and estimates age and gender for the detected faces.",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--weight_file", type=str, default=None,
                        help="path to weight file (e.g. weights.18-4.06.hdf5)")
    parser.add_argument("--depth", type=int, default=16,
                        help="depth of network")
    parser.add_argument("--width", type=int, default=8,
                        help="width of network")
    args = parser.parse_args()
    return args


def do_recognize_person(face_names, fid):
    time.sleep(1)
    face_names[fid] = "Person " + str(fid)


def face_tracking_estimation_module():
    args = get_args()
    depth = args.depth
    k = args.width
    weight_file = 'C:/Users/KWON/PycharmProjects/Smart-Visitor-Pass/deep_learning/age-gender-estimation/fine_tuned_model/WRN_16_8_2.h5'
    '''
    if not weight_file:
        weight_file = get_file("WRN_16_8_2.h5",
                               pretrained_model, cache_subdir="pretrained_models",
                               file_hash=modhash, cache_dir=os.path.dirname(os.path.abspath(__file__)))
'''
    # load model and weights
    img_size = 64
    model = WideResNet(img_size, depth=depth, k=k)()
    model.load_weights(weight_file)

    # Open the first web cam device
    capture = cv2.VideoCapture(1) # web cam number(default = 0)

    # Create two opencv named windows
    # cv2.namedWindow("base-image", cv2.WINDOW_AUTOSIZE)
    # cv2.namedWindow("result-image", cv2.WINDOW_AUTOSIZE)

    # Position the windows next to each other
    # cv2.moveWindow("base-image", 0, 100)
    # cv2.moveWindow("result-image", 400, 100)

    # Start the window thread for the two windows we are using
    # cv2.startWindowThread()

    # The color of the rectangle we draw around the face
    rectangle_color = (0, 165, 255)  # orange

    # variables holding the current frame number and the current faceid
    frameCounter = 0
    currentFaceID = 0

    # Variables holding the correlation trackers and the name per faceid
    faceTrackers = {}
    faceNames = {}

    try:
        while True:
            # Retrieve the latest image from the webcam
            rc, fullSizeBaseImage = capture.read()

            # Resize the image to 320x240
            # baseImage = cv2.resize(fullSizeBaseImage, (320, 240))
            ## if image size is too small, estimation rate decreases
            baseImage = cv2.resize(fullSizeBaseImage, (640, 480))

            # Check if a key was pressed and if it was Q, then break
            # from the infinite loop
            pressedKey = cv2.waitKey(2)
            if pressedKey == ord('Q'):
                break

            # Result image is the image we will show the user, which is a
            # combination of the original image from the webcam and the
            # overlayed rectangle for the largest face

            resultImage = baseImage.copy()

            # STEPS:
            # * Update all trackers and remove the ones that are not
            #   relevant anymore
            # * Every 10 frames:
            #       + Use face detection on the current frame and look
            #         for faces.
            #       + For each found face, check if centerpoint is within
            #         existing tracked box. If so, nothing to do
            #       + If centerpoint is NOT in existing tracked box, then
            #         we add a new tracker with a new face-id

            # Increase the framecounter
            frameCounter += 1

            # Update all the trackers and remove the ones for which the update
            # indicated the quality was not good eno
            # gh
            fidsToDelete = []

            for fid in faceTrackers.keys():
                trackingQuality = faceTrackers[fid].update(baseImage)

                # If the tracking quality is good enough, we must delete
                # this tracker
                if trackingQuality < 7:
                    fidsToDelete.append(fid)

            for fid in fidsToDelete:
                print("Removing fid " + str(fid) + " from list of trackers")
                faceTrackers.pop(fid, None)

            # Every 10 frames, we will have to determine which faces
            # are present in the frame
            if (frameCounter % 10) == 0:

                # For the face detection, we need to make use of a gray
                # colored image so we will convert the baseImage to a
                # gray-based image

                # gray = cv2.cvtColor(baseImage, cv2.COLOR_BGR2GRAY)
                # img_h, img_w = np.shape(gray)
                gray = cv2.cvtColor(baseImage, cv2.COLOR_BGR2RGB)
                img_h, img_w, _ = np.shape(gray)
                # Now use the haar cascade detector to find all faces
                # in the image
                faces = faceCascade.detectMultiScale(gray, 1.3, 5)
                faces_arr = np.empty((len(faces), img_size, img_size, 3))
                # label = [['sample_id', 'sample_result']] * len(faces)
                label = []

                # Loop over all faces and check if the area for this
                # face is the largest so far
                # We need to convert it to int here because of the
                # requirement of the dlib tracker. If we omit the cast to
                # int here, you will get cast errors since the detector
                # returns numpy.int32 and the tracker requires an int

                for (_x, _y, _w, _h) in faces:

                    x = int(_x)
                    y = int(_y)
                    w = int(_w)
                    h = int(_h)

                    # calculate the centerpoint
                    x_bar = x + 0.5 * w
                    y_bar = y + 0.5 * h

                    # Variable holding information which faceid we
                    # matched with
                    matchedFid = None

                    # Now loop over all the trackers and check if the
                    # centerpoint of the face is within the box of a
                    # tracker

                    for fid in faceTrackers.keys():
                        tracked_position = faceTrackers[fid].get_position()

                        t_x = int(tracked_position.left())
                        t_y = int(tracked_position.top())

                        t_x2 = int(tracked_position.right()) + 1
                        t_y2 = int(tracked_position.bottom()) + 1

                        t_w = int(tracked_position.width())
                        t_h = int(tracked_position.height())

                        # calculate the centerpoint
                        t_x_bar = t_x + 0.5 * t_w
                        t_y_bar = t_y + 0.5 * t_h

                        # check if the centerpoint of the face is within the
                        # rectangle of a tracker region. Also, the centerpoint
                        # of the tracker region must be within the region
                        # detected as a face. If both of these conditions hold
                        # we have a match
                        if ((t_x <= x_bar <= (t_x + t_w)) and
                                (t_y <= y_bar <= (t_y + t_h)) and
                                (x <= t_x_bar <= (x + w)) and
                                (y <= t_y_bar <= (y + h))):
                            matchedFid = fid

                        xw1 = max(int(t_x - 0.4 * t_w), 0)
                        yw1 = max(int(t_y - 0.4 * t_h), 0)
                        xw2 = min(int(t_x2 + 0.4 * t_w), img_w - 1)
                        yw2 = min(int(t_y2 + 0.4 * t_h), img_h - 1)

                        faces_arr[0, :, :, :] = cv2.resize \
                            (baseImage[yw1:yw2 + 1, xw1:xw2 + 1, :], (img_size, img_size))

                        results = model.predict(faces_arr)
                        predicted_genders = results[0]
                        ages = np.arange(0, 101).reshape(101, 1)
                        predicted_ages = results[1].dot(ages).flatten()

                        result_age = int(predicted_ages[0])
                        result_gender = "F" if predicted_genders[0][0] > 0.5 else "M"
                        estimation_timestamp = strftime('%Y-%m-%d %H:%M:%S', localtime())

                        data_trim(fid, result_age, result_gender, estimation_timestamp)
                        # label = label[:fid] + [fid, "{}, {}".format(int(predicted_ages[0]), "F" if predicted_genders[0][0] > 0.5 else "M")] + label[fid:]
                        label.append([fid, "{}, {}".format(result_age, result_gender)])



                    # If no matched fid, then we have to create a new tracker
                    if matchedFid is None:
                        print("Creating new tracker " + str(currentFaceID))

                        # Create and st
                        # ore the tracker
                        tracker = dlib.correlation_tracker()

                        tracker.start_track(baseImage,
                                            dlib.rectangle(x - 10,
                                                           y - 20,
                                                           x + w + 10,
                                                           y + h + 20)
                                            )

                        faceTrackers[currentFaceID] = tracker

                        # Start a new thread that is used to simulate
                        # face recognition. This is not yet implemented in this
                        # version :)
                        t = threading.Thread(target=do_recognize_person,
                                             args=(faceNames, currentFaceID))
                        t.start()

                        # Increase the currentFaceID counter
                        currentFaceID += 1

            # Now loop over all the trackers we have and draw the rectangle
            # around the detected faces. If we 'know' the name for this person
            # (i.e. the recognition thread is finished), we print the name
            # of the person, otherwise the message indicating we are detecting
            # the name of the person

            for fid in faceTrackers.keys():
                tracked_position = faceTrackers[fid].get_position()

                t_x = int(tracked_position.left())
                t_y = int(tracked_position.top())

                t_w = int(tracked_position.width())
                t_h = int(tracked_position.height())

                cv2.rectangle(resultImage, (t_x, t_y),
                              (t_x + t_w, t_y + t_h),
                              rectangle_color, 2)
        
                if fid in faceNames.keys():
                    for i in range(len(label)):
                        if label[i][0] == fid:
                            result_with_fid = label[i]
                    try:
                        cv2.putText(resultImage, faceNames[fid] + ' ' + result_with_fid[1],
                                    (int(t_x + t_w / 2), int(t_y)),
                                    cv2.FONT_HERSHEY_SIMPLEX,
                                    0.5, (255, 255, 255), 2)
                    except UnboundLocalError as e:
                        pass
                else:
                    cv2.putText(resultImage, "Detecting...",
                                (int(t_x + t_w / 2), int(t_y)),
                                cv2.FONT_HERSHEY_SIMPLEX,
                                0.5, (255, 255, 255), 2)

            # Since we want to show something larger on the screen than the
            # original 320x240, we resize the image again
            #
            # Note that it would also be possible to keep the large version
            # of the baseimage and make the result image a copy of this large
            # base image and use the scaling factor to draw the rectangle
            # at the right coordinates.
            largeResult = cv2.resize(resultImage,
                                     (OUTPUT_SIZE_WIDTH, OUTPUT_SIZE_HEIGHT))

            # Finally, we want to show the images on the screen
            # cv2.imshow("base-image", baseImage)
            cv2.imshow("result-image", largeResult)

    # To ensure we can also deal with the user pressing Ctrl-C in the console
    # we have to check for the KeyboardInterrupt exception and break out of
    # the main loop
    except KeyboardInterrupt as e:
        pass

    # Destroy any OpenCV windows and exit the application
    cv2.destroyAllWindows()
    exit(0)


def data_trim(fid, estimated_age, estimated_gender, estimation_timestamp):
    global result_df
    unique_id = str(fid) + '_' + estimation_timestamp

    current_df = pd.DataFrame([[unique_id, fid, estimated_age, estimated_gender, estimation_timestamp]], columns=columns)

    # if fid in result_df['face_id'].tolist():
    #    result_df = result_df.drop((result_df['face_id'] == fid).index)

    # result_df = result_df.append(current_df, ignore_index=True)

    store_data_in_db(current_df)


def store_data_in_db(data_df):
    data_values = "('" + str(data_df['id'][0]) + "','" + str(data_df['face_id'][0]) + "','" + str(
        data_df['age'][0]) + "','" + str(data_df['gender'][0]) \
                  + "','" + str(data_df['timestamp'][0]) + "')"
    print(data_values)

    try:
        cur = conn.cursor()
        cur.execute('delete from visitor_info where face_id= ' + data_values.split(',')[1] + ';')
        cur.execute('insert into visitor_info values ' + data_values + ';')

        conn.commit()
        print(cur.fetchone())
    except pymysql.err.OperationalError as e:
        print(e)
    except Exception as e:
        print(e)


if __name__ == '__main__':
    face_tracking_estimation_module()

