import numpy as np
import cv2
import scipy.io
import argparse
from tqdm import tqdm
from utils import get_meta
import pandas as pd

# create dataset
# python3 create_db.py --output data/imdb_db.mat --db imdb --img_size 64


def get_args():
    parser = argparse.ArgumentParser(description="This script cleans-up noisy labels "
                                                 "and creates database for training.",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--output", "-o", type=str, required=True,
                        help="path to output database mat file")
    parser.add_argument("--db", type=str, default="wiki",
                        help="dataset; wiki or imdb")
    parser.add_argument("--img_size", type=int, default=32,
                        help="output image size")
    parser.add_argument("--min_score", type=float, default=1.0,
                        help="minimum face_score")
    args = parser.parse_args()
    return args


def main():
    output_path = "./data/AFD_db.mat"
    db = "AFD"
    img_size = 64

    metadata_df = pd.read_csv("C:/Users/cvlab/Desktop/team21//asian_face_metadata.csv")

    root_path = "C:/Users/cvlab/Desktop/team21/AFD- Accessory/"
    # mat_path = root_path + "{}.mat".format(db)
    # full_path, dob, gender, photo_taken, face_score, second_face_score, age = get_meta(mat_path, db)
    full_path = metadata_df['image'].tolist()
    out_genders = metadata_df['gender'].tolist()
    print(out_genders)
    out_ages = metadata_df['age'].tolist()
    out_imgs = []

    for i in range(len(metadata_df)):
        '''
        if face_score[i] < min_score:
            continue

        if (~np.isnan(second_face_score[i])) and second_face_score[i] > 0.0:
            continue

        if ~(0 <= age[i] <= 100):
            continue

        if np.isnan(gender[i]):
            continue
        '''
        img = cv2.imread(root_path + str(full_path[i]))
        out_imgs.append(cv2.resize(img, (img_size, img_size)))

    output = {"image": np.array(out_imgs), "gender": np.array(out_genders), "age": np.array(out_ages),
              "db": db, "img_size": img_size}
    scipy.io.savemat(output_path, output)


if __name__ == '__main__':
    main()
