import pandas as pd

metadata_df = pd.read_csv('asian_face_metadata_source.csv')

age_scope_list = []
gender_list = []

for i in range(len(metadata_df)):
    gender_list.append(list(metadata_df.loc[i][0])[0])
    age_scope_list.append(list(metadata_df.loc[i][0])[1])

gender_se = pd.Series(gender_list)
age_scope_se = pd.Series(age_scope_list)

metadata_df["gender"] = gender_se.values
metadata_df["ratio"] = age_scope_se.values

print(metadata_df)

metadata_df.to_csv('asian_face_metadata_label.csv')