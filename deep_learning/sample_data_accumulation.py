import pandas as pd
from time import localtime, strftime
import random
import pymysql

columns = ['id', 'face_id', 'age', 'gender', 'timestamp']
result_df = pd.DataFrame(columns=columns)

conn = pymysql.connect(host='smartvisitorpass.cwbrhvpulmkd.ap-northeast-2.rds.amazonaws.com',
                       port=3306, user='root', passwd='team21sangha', db='smart_visitor_pass', charset='utf8')
cur = conn.cursor()

for i in range(10000):
    sample_fid = i

    sample_date = str(random.randrange(1, 32)).rjust(2, '0')
    sample_hour = str(random.randrange(0, 24)).rjust(2, '0')
    sample_minute = str(random.randrange(0, 60)).rjust(2, '0')
    sample_second = str(random.randrange(0, 60)).rjust(2, '0')

    sample_timestamp = strftime('%Y-%m', localtime()) + "-" + str(sample_date) + " " + str(sample_hour) \
                       + ":" + str(sample_minute)+ ":" + str(sample_second)

    sample_id = str(i) + "_" + sample_timestamp
    random_age = str(random.randrange(10, 80))
    random_gender = random.choice("FM")
    random_location = str(random.randrange(0, 5))

    sample_dict = {"id":[sample_id], "face_id":[sample_fid], "age":[random_age],
                   "gender":[random_gender], "timestamp":[sample_timestamp], "location":[random_location]}
    sample_df = pd.DataFrame(sample_dict, columns=['id', 'face_id', 'age', 'gender', 'timestamp', 'location'])
    print(i)

    data_values = "('" + str(sample_df['id'][0]) + "','" + str(sample_df['face_id'][0]) + "','" + str(
        sample_df['age'][0]) + "','" + str(sample_df['gender'][0]) \
                  + "','" + str(sample_df['timestamp'][0]) + "','" + str(sample_df['location'][0])+ "')"

    cur.execute('insert into visitor_info_sample values ' + data_values + ';')

    conn.commit()

    i = i + 1
    # print(cur.fetchone())

    # result_df = result_df.append(sample_df, ignore_index=True)

