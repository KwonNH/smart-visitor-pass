import pandas as pd

group_labeled_data = pd.read_csv('asian_face_metadata_group_labeling.csv')

source_df = group_labeled_data

age_i = 0

for i in range(len(source_df)):
    age_nh = source_df['age_nh'][i]
    age_hy = source_df['age_hy'][i]
    age_sh = source_df['age_sh'][i]

    if age_nh == age_hy and age_hy == age_sh:
        age_i = age_nh
    elif age_nh == age_hy:
        age_i = age_nh
    elif age_nh == age_sh:
        age_i = age_nh
    elif age_hy == age_sh:
        age_i = age_hy
    else:
        age_i = -1

    source_df.set_value(i, 'age', age_i)

source_df.to_csv('asian_face_metadata.csv')
